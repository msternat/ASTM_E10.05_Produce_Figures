E704 --- Figure Generation
==========================

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 704.  The recommended citation for this standard is:

* ASTM E704-13, Standard Test Method for Measuring Reaction Rates by
  Radioactivation of Uranium-238, ASTM International, West Conshohocken, PA,
  2013, www.astm.org

For this standard, one figure is required.  The process to generate this figure
is described next.

Figure 1
--------

.. Code Documentation
.. ------------------

.. To be added...

.. .. automodule:: scripts_generate_figures.E_0704
..    :members:

.. Indices and tables
.. -----------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

