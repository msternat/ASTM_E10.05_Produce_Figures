E944
====

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 944.  The recommended citation for this standard is:

* ASTM E944-13e1, Standard Guide for Application of Neutron Spectrum Adjustment
  Methods in Reactor Surveillance, ASTM International, West Conshohocken, PA,
  2013, www.astm.org

