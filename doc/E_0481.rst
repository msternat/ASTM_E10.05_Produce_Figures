E481
====

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 481.  The recommended citation for this standard is:

* ASTM E481-16, Standard Test Method for Measuring Neutron Fluence Rates by
  Radioactivation of Cobalt and Silver, ASTM International, West Conshohocken,
  PA, 2016, www.astm.org

