E261
====

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 261.  The recommended citation for this standard is:

* ASTM E261-16, Standard Practice for Determining Neutron Fluence, Fluence Rate,
  and Spectra by Radioactivation Techniques, ASTM International, West
  Conshohocken, PA, 2016, www.astm.org

