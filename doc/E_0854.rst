E854
====

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 854.  The recommended citation for this standard is:

* ASTM E854-14e1, Standard Test Method for Application and Analysis of Solid
  State Track Recorder (SSTR) Monitors for Reactor Surveillance, ASTM
  International, West Conshohocken, PA, 2014, www.astm.org

