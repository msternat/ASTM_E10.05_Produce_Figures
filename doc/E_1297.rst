E1297 --- Figure Generation
===========================

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 1297.  The recommended citation for this standard is:

* ASTM E1297-08(2013), Standard Test Method for Measuring Fast-Neutron Reaction
  Rates by Radioactivation of Niobium, ASTM International, West Conshohocken,
  PA, 2008, www.astm.org

For this standard, one figure is required.  The process to generate this figure
is described next.

Figure 1
--------

.. Code Documentation
.. ------------------

.. To be added...

.. .. automodule:: scripts_generate_figures.E_1297
..    :members:

.. Indices and tables
.. -----------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

