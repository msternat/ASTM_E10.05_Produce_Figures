E2006
=====

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 2006.  The recommended citation for this standard is:

* ASTM E2006-16, Standard Guide for Benchmark Testing of Light Water Reactor
  Calculations, ASTM International, West Conshohocken, PA, 2016, www.astm.org

