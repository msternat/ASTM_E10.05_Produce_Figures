#!/usr/bin/env python

# import matplotlib as mpl
# mpl.use('agg')
# import matplotlib.font_manager
# [ print(f) for f in matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')  ]

# import matplotlib.font_manager
# flist = matplotlib.font_manager.get_fontconfig_fonts()
# names = [matplotlib.font_manager.FontProperties(fname=fname).get_name() for fname in flist]
# print( names )

import scripts_generate_figures.E_0705 as E_0705
import scripts_generate_figures.E_0266 as E_0266

# Generate figures.
E_0705.make_plots( destination = './doc/figures_generated' )
E_0266.make_plots( destination = './doc/figures_generated' )

